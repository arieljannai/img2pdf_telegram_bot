import img2pdf
import urllib.request
import tempfile
import os


class Pdf:
    file_name = ''

    def __init__(self, friendly_name=None, caption=None):
        self.friendly_name = friendly_name
        self.caption = caption
        self.photos = list()

    def add(self, args):
        self.photos.extend(self.__listify(args))

    def add_from_links(self, links):
        self.photos.extend(self.__load_files(self.__listify(links)))

    def generate(self):
        if not self.photos:
            return None

        with tempfile.NamedTemporaryFile(suffix='.pdf', delete=False) as pdf:
            pdf.write(img2pdf.convert(self.photos))
            self.file_name = pdf.name

        return open(self.file_name, 'rb')

    def clean(self):
        if self.file_name:
            try:
                os.remove(self.file_name)
            except OSError:
                pass

    def __del__(self):
        self.clean()

    def __len(self):
        return len(self.photos)

    def __load_file(self, photo_url):
        return self.__read_file(photo_url)

    def __load_files(self, photos_url):
        return map(self.__read_file, photos_url)

    @staticmethod
    def __read_file(file_url):
        return urllib.request.urlopen(file_url).read()

    @staticmethod
    def __listify(args):
        if type(args) is list:
            return args
        if type(args) is str:
            return [args]





