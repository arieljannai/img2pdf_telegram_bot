#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import logging
import os
import sys
import gettext
import json
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from convert import Pdf


def load_config():
    with open('config.json') as conf:
        return json.load(conf)


def load_token():
    with open('token', 'r') as t:
        return t.read()

config = load_config()

# if sys.platform.startswith('win'):
#     if os.getenv('LANG') is None:
#         import locale
#         lang, enc = locale.getdefaultlocale()
#         os.environ['LANG'] = lang

os.environ['LANG'] = config['translations']['default_lang']

t = gettext.translation('img2pdf', "./translations/locale", fallback=True)
_ = t.gettext
_n = t.ngettext

# print(_('welcome_greeting'))
# print(_('hi'))
# print(_n('have_apple', 'have_apples', 3) % {'num': 3})




logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

logger = logging.getLogger(__name__)

pdfs = dict()


def start(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=_('bot_start_message'))


def new_pdf(bot, update, args):
    if not args:
        pdf_name = 'document'
    else:
        pdf_name = args[0]

    caption = ' '.join(args[1:]) if len(args) > 1 else None

    pdf_name += '' if pdf_name.endswith('.pdf') else '.pdf'
    pdfs[update.message.chat_id] = Pdf(pdf_name, caption)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=_('start_add_to_pdf').format(pdf_name=pdf_name))


def image_handler(bot, update):
    if update.message.chat_id in pdfs:
        pdf = pdfs[update.message.chat_id]
        photo = update.message.photo[-1]
        photo_file = bot.getFile(photo.file_id)
        pdf.add_from_links(photo_file.file_path)

        bot.sendMessage(chat_id=update.message.chat_id,
                        reply_to_message_id=update.message.message_id,
                        text=_('pdf_images_add_count').format(amount=len(pdf)))
    else:
        bot.sendMessage(
            chat_id=update.message.chat_id,
            reply_to_message_id=update.message.message_id,
            text=_('create_pdf_before_add'))


def generate(bot, update):
    if update.message.chat_id in pdfs:
        pdf = pdfs[update.message.chat_id]
        bot.sendMessage(
            chat_id=update.message.chat_id,
            reply_to_message_id=update.message.message_id,
            text=_('generating_please_wait').format(
                pdf_name=pdf.friendly_name, amount=len(pdf)))

        file = pdf.generate()
        bot.sendDocument(chat_id=update.message.chat_id,
                         document=file, caption=pdf.caption,
                         filename=pdf.friendly_name)
    else:
        bot.sendMessage(
            chat_id=update.message.chat_id,
            reply_to_message_id=update.message.message_id,
            text=_('nothing_to_generate'))


def finish(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id,
                    reply_to_message_id=update.message.message_id,
                    text=_('op_canceled'))
    del pdfs[update.message.chat_id]


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


def main():
    updater = Updater(token=load_token())
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(MessageHandler(Filters.photo, image_handler))
    dispatcher.add_handler(CommandHandler('generate', generate))
    dispatcher.add_handler(CommandHandler('new', new_pdf, pass_args=True))
    dispatcher.add_handler(CommandHandler('finish', finish))
    dispatcher.add_handler(CommandHandler('cancel', finish))
    dispatcher.add_error_handler(error)

    updater.start_polling(timeout=1)
    updater.idle()


if __name__ == '__main__':
    main()
